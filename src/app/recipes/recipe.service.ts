import { Injectable } from '@angular/core';

import { Recipe } from './recipe.model';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../shopping-list/shopping-list.service';

@Injectable()

export class RecipeService {
    
    private recipes : Recipe[] = [
        new Recipe(
            1,
            'Garlicky Mushroom Toast Cups', 
            'Delicious, garlicky mushrooms in crispy buttery toast cups.', 
            'https://images.media-allrecipes.com/userphotos/250x250/126695.jpg', 
            [
                new Ingredient('Mushroom', '1'),
                new Ingredient('F Fry', '10')
            ]
        ),
        new Recipe(
            2,
            'Chicken Rice', 
            'Delicious, garlicky chicken and busmati rice', 
            'https://www.indianhealthyrecipes.com/wp-content/uploads/2019/05/chicken-fried-rice.jpg', 
            [
                new Ingredient('Extra Chiocken', '1'),
                new Ingredient('F Fry', '10')
            ]
        )
    ];

    constructor(private slService: ShoppingListService){}

    getRecipes(){
        return this.recipes.slice();
    }

    getRecipe(index: number){
        return this.recipes[index];
    }

    addIngredietaToShoppingList(ingredients: Ingredient[]){
        this.slService.addIngredients(ingredients);
    }
}